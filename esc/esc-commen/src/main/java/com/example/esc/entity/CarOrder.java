package com.example.esc.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;


/**
 * <p>
 * 
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
@Data
@TableName("car_order")
public class CarOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer oid;

    private Integer number;

    private Integer uid;

    private Integer carId;

    private LocalDate time;

    private String price;

      /**
     * 已付款、提车
     */
      private String state;

    

}
