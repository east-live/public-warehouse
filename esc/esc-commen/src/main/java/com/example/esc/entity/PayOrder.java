package com.example.esc.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * 
 * </p>
 *
 * @author baomidou
 * @since 2023-04-19
 */
@Data
@TableName("pay_order")
public class PayOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    private CarMessage carMes;

      private String poid;

    private Integer uid;

    private String username;

    private Integer carId;

    private String introduction;

    //@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDate time;

    private String price;

    private Integer number;

      /**
     * 已付款、提车
     */
      private String state;

    
    public String getPoid() {
        return poid;
    }

      public void setPoid(String poid) {
          this.poid = poid;
      }
    
    public Integer getUid() {
        return uid;
    }

      public void setUid(Integer uid) {
          this.uid = uid;
      }
    
    public String getUsername() {
        return username;
    }

      public void setUsername(String username) {
          this.username = username;
      }
    
    public Integer getCarName() {
        return carId;
    }

      public void setCarName(Integer carName) {
          this.carId = carName;
      }
    
    public String getIntroduction() {
        return introduction;
    }

      public void setIntroduction(String introduction) {
          this.introduction = introduction;
      }
    
    public LocalDate getTime() {
        return time;
    }

      public void setTime(LocalDate time) {
          this.time = time;
      }
    
    public String getPrice() {
        return price;
    }

      public void setPrice(String price) {
          this.price = price;
      }
    
    public Integer getNumber() {
        return number;
    }

      public void setNumber(Integer number) {
          this.number = number;
      }
    
    public String getState() {
        return state;
    }

      public void setState(String state) {
          this.state = state;
      }

    @Override
    public String toString() {
        return "PayOrder{" +
              "poid=" + poid +
                  ", uid=" + uid +
                  ", username=" + username +
                  ", carId=" + carId +
                  ", introduction=" + introduction +
                  ", time=" + time +
                  ", price=" + price +
                  ", number=" + number +
                  ", state=" + state +
              "}";
    }
}
