package com.example.esc.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;


/**
 * <p>
 * 
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
@Data
@TableName("car_message")
public class CarMessage implements Serializable {

    private static final long serialVersionUID = 1L;


    private User user;

    private Integer carId;

    private String carName;

    private String type;

    private String oldPrice;

    private String newPrice;

    private String introduction;

    private LocalDate date;

    private String image;

    private String traveled;

    private String disp;

    private String emission;

    private String state;

    private Integer uid;

    
    public Integer getCarId() {
        return carId;
    }

      public void setCarId(Integer carId) {
          this.carId = carId;
      }
    
    public String getCarName() {
        return carName;
    }

      public void setCarName(String carName) {
          this.carName = carName;
      }
    
    public String getType() {
        return type;
    }

      public void setType(String type) {
          this.type = type;
      }
    
    public String getOldPrice() {
        return oldPrice;
    }

      public void setOldPrice(String oldPrice) {
          this.oldPrice = oldPrice;
      }
    
    public String getNewPrice() {
        return newPrice;
    }

      public void setNewPrice(String newPrice) {
          this.newPrice = newPrice;
      }
    
    public String getIntroduction() {
        return introduction;
    }

      public void setIntroduction(String introduction) {
          this.introduction = introduction;
      }
    
    public LocalDate getDate() {
        return date;
    }

      public void setDate(LocalDate date) {
          this.date = date;
      }
    
    public String getImage() {
        return image;
    }

      public void setImage(String image) {
          this.image = image;
      }
    
    public String getTraveled() {
        return traveled;
    }

      public void setTraveled(String traveled) {
          this.traveled = traveled;
      }
    
    public String getDisp() {
        return disp;
    }

      public void setDisp(String disp) {
          this.disp = disp;
      }
    
    public String getEmission() {
        return emission;
    }

      public void setEmission(String emission) {
          this.emission = emission;
      }
    
    public String getState() {
        return state;
    }

      public void setState(String state) {
          this.state = state;
      }
    
    public Integer getUid() {
        return uid;
    }

      public void setUid(Integer uid) {
          this.uid = uid;
      }

    @Override
    public String toString() {
        return "CarMessage{" +
              "carId=" + carId +
                  ", carName=" + carName +
                  ", type=" + type +
                  ", oldPrice=" + oldPrice +
                  ", newPrice=" + newPrice +
                  ", introduction=" + introduction +
                  ", date=" + date +
                  ", image=" + image +
                  ", traveled=" + traveled +
                  ", disp=" + disp +
                  ", emission=" + emission +
                  ", state=" + state +
                  ", uid=" + uid +
              "}";
    }
}
