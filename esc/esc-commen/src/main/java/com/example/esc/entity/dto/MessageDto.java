package com.example.esc.entity.dto;

import com.example.esc.entity.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageDto extends Message {
    private Integer carId;

    private String carName;

    private String type;

    private String oldPrice;

    private String newPrice;

    private String introduction;

    private LocalDate date;

    private String image;

    private String traveled;

    private String disp;

    private String emission;

    private String state;

    private Integer uid;

}
