package com.example.esc.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Shouhuodizhi implements Serializable {

    private static final long serialVersionUID = 1L;

      private Integer sid;

    private String username;

    private String phone;

    private String addr;

    private String xiangxiaddr;

    private Integer carId;

    private String youbian;

    private Integer uid;

    @TableField("is_default")
    private Integer isDefault;

}
