package com.example.esc.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;


/**
 * <p>
 * 
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
@Data
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    private User user;

      private Integer mid;

    private Integer carId;

    private String content;

    private Integer uid;

    private LocalDate time;

    
    public Integer getMid() {
        return mid;
    }

      public void setMid(Integer mid) {
          this.mid = mid;
      }
    
    public Integer getCarId() {
        return carId;
    }

      public void setCarId(Integer carId) {
          this.carId = carId;
      }
    
    public String getContent() {
        return content;
    }

      public void setContent(String content) {
          this.content = content;
      }
    
    public Integer getUid() {
        return uid;
    }

      public void setUid(Integer uid) {
          this.uid = uid;
      }
    
    public LocalDate getTime() {
        return time;
    }

      public void setTime(LocalDate time) {
          this.time = time;
      }

    @Override
    public String toString() {
        return "Message{" +
              "mid=" + mid +
                  ", carId=" + carId +
                  ", content=" + content +
                  ", uid=" + uid +
                  ", time=" + time +
              "}";
    }
}
