package com.example.esc.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

      private Integer uid;

    private String username;

    private String password;

    private String email;

    private String phone;

    private String image;

    private String sex;

    private String state;

    private String addr;

    private Integer mid;

    private Integer carId;

    private String youbian;

    
    public Integer getUid() {
        return uid;
    }

      public void setUid(Integer uid) {
          this.uid = uid;
      }
    
    public String getUsername() {
        return username;
    }

      public void setUsername(String username) {
          this.username = username;
      }
    
    public String getPassword() {
        return password;
    }

      public void setPassword(String password) {
          this.password = password;
      }
    
    public String getEmail() {
        return email;
    }

      public void setEmail(String email) {
          this.email = email;
      }
    
    public String getPhone() {
        return phone;
    }

      public void setPhone(String phone) {
          this.phone = phone;
      }
    
    public String getImage() {
        return image;
    }

      public void setImage(String image) {
          this.image = image;
      }
    
    public String getSex() {
        return sex;
    }

      public void setSex(String sex) {
          this.sex = sex;
      }
    
    public String getState() {
        return state;
    }

      public void setState(String state) {
          this.state = state;
      }
    
    public String getAddr() {
        return addr;
    }

      public void setAddr(String addr) {
          this.addr = addr;
      }
    
    public Integer getMid() {
        return mid;
    }

      public void setMid(Integer mid) {
          this.mid = mid;
      }
    
    public Integer getCarId() {
        return carId;
    }

      public void setCarId(Integer carId) {
          this.carId = carId;
      }

    @Override
    public String toString() {
        return "User{" +
              "uid=" + uid +
                  ", username=" + username +
                  ", password=" + password +
                  ", email=" + email +
                  ", phone=" + phone +
                  ", image=" + image +
                  ", sex=" + sex +
                  ", state=" + state +
                  ", addr=" + addr +
                  ", mid=" + mid +
                  ", carId=" + carId +
              "}";
    }
}
