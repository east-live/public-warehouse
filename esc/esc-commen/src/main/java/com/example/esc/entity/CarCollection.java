package com.example.esc.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.sql.Date;
import java.time.LocalDate;


@TableName("car_collection")
public class CarCollection {
    private static final long serialVersionUID = 1L;

    private CarMessage CarM;

    private Integer coid;

    private Integer uid;

    private Integer carId;

    private LocalDate carTime;

    public CarMessage getCarM() {
        return CarM;
    }

    public void setCarM(CarMessage carM) {
        CarM = carM;
    }

    public Integer getCoid() {
        return coid;
    }

    public void setCoid(Integer coid) {
        this.coid = coid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public LocalDate getCarTime() {
        return carTime;
    }

    public void setCarTime(LocalDate carTime) {
        this.carTime = carTime;
    }

    public CarCollection() {
        super();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public String toString() {
        return "CarCollection{" +
                "CarM=" + CarM +
                ", coid=" + coid +
                ", uid=" + uid +
                ", carId=" + carId +
                ", carTime=" + carTime +
                '}';
    }
}