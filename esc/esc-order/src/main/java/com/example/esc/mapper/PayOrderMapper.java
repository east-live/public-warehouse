package com.example.esc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.esc.entity.CarMessage;
import com.example.esc.entity.Message;
import com.example.esc.entity.PayOrder;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface PayOrderMapper extends BaseMapper<PayOrder> {

    //生成订单
    @Insert("insert into pay_order(poid,uid,username,carId,time,price,introduction,number,state) values(#{poid},#{uid},#{username}, #{carId},#{time},#{price},#{introduction},#{number},#{state})")
    int insorder(PayOrder payOrder);

    //查询订单
    @Select("select * from pay_order where uid = #{uid}")
    @Results(
            value = {
                    @Result(column = "carId", property = "carId", id = true),
                    @Result(column = "carId", property = "carMes", many = @Many(select = "selectCar")),
            }
    )
    List<PayOrder> selOrder(@Param("uid") int uid);

    @Select("select * from car_message where carId = #{carId}")
    CarMessage selectCar(@Param("carId") int carId);

    @Delete("delete from pay_order where poid=#{poid}")
    int delOrder(@Param("poid") String poid);

}
