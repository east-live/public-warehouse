package com.example.esc.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.esc.entity.CarMessage;
import com.example.esc.feign.FeignUser;
import com.example.esc.mapper.CarMessageMapper;
import com.example.esc.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
@RestController
@RequestMapping("/esc/carMessage")
public class CarMessageController {

    @Resource
    private CarMessageMapper carMessageMapper;

    @Resource
    private FeignUser feignUser;

    @Autowired
    private RedisTemplate redisTemplate;


    //test
    @GetMapping("/getId")
    public int getId(){
        return feignUser.getId();
    }

    //首页
    @GetMapping("queryCar")
    public List<CarMessage> queryCar(){

        String key = "index.html";
        //先从redis中获取缓存数据
        List<CarMessage> carMessages = (List<CarMessage>) redisTemplate.opsForValue().get(key);
        // 如果存在， 直接返回， 无需查询数据库
        if(carMessages != null) {
            return carMessages;
        }
        carMessages = carMessageMapper.selectCar();

        // 将首页数据存入redis
        redisTemplate.opsForValue().set(key, carMessages, 30, TimeUnit.MINUTES);
        return carMessages;
    }

    //车辆详情页
    @GetMapping("queryDet")
    public CarMessage queryDet(@RequestParam String carId){

        return carMessageMapper.selectDet(carId);
    }

    //买车页面分页
    @PostMapping("queryPage")
    public Page<CarMessage> queryPage(
            @RequestParam(defaultValue = "1") int pageNum,
            @RequestParam(defaultValue = "12") int size){
        String key = "buy_car";
        //先从redis中获取缓存数据
        Page<CarMessage> value = (Page<CarMessage>)redisTemplate.opsForValue().get(key);
        //如果存在， 直接返回， 无需查询数据库
        if(value != null) {
            return value;
        }
        List<CarMessage> list = carMessageMapper.selectPagesed((pageNum - 1) * size, size);
        long count = carMessageMapper.countPage();
        double a =12;
        double y = count/a;
        double ym = Math.ceil(y);
        Page<CarMessage> p = new Page<>(pageNum,size);
        p.setTotal((long) ym);
        p.setRecords(list);
        // 将查到的数据缓存到Redis中， 并且设置有效期为30分钟
        redisTemplate.opsForValue().set(key, p, 30, TimeUnit.MINUTES);
        return p;
    }

    @PostMapping("queryType")
    public List<CarMessage> queryType(@RequestParam String type){
        return carMessageMapper.selectType(type);
    }

    //发布车源
    @PostMapping("/fabucheyuan")
    public Result fabucheyuan(@RequestBody CarMessage carMessage) {
        carMessage.setDate(LocalDate.now());
        carMessage.setDisp("2.0T");
        carMessage.setEmission("国六");
        carMessage.setState("1");
        System.out.println(carMessage);
        carMessageMapper.insCarMessage(carMessage);
        return new Result(1, "发布成功", null);
    }


}









