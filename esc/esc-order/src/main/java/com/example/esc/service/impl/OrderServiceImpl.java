package com.example.esc.service.impl;

import com.example.esc.entity.CarOrder;
import com.example.esc.mapper.OrderMapper;
import com.example.esc.service.IOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, CarOrder> implements IOrderService {

}
