package com.example.esc.controller;

import com.example.esc.entity.PayOrder;
import com.example.esc.mapper.PayOrderMapper;
import com.example.esc.vo.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/esc/payOrder")
public class PayOrderController {

    @Resource
    private PayOrderMapper payOrderMapper;

    //生成订单
    @PostMapping("payOrder")
    public Result payOrder(@RequestParam String poid,
                           @RequestParam int uid,
                           @RequestParam String username,
                           @RequestParam int carId,
                           @RequestParam String price,
                           @RequestParam String introduction,
                           @RequestParam(defaultValue = "已付款") String state){
        PayOrder payOrder = new PayOrder();
        payOrder.setPoid(poid);
        payOrder.setUid(uid);
        payOrder.setUsername(username);
        payOrder.setCarName(carId);
        payOrder.setTime(LocalDate.now());
        payOrder.setPrice(price);
        payOrder.setIntroduction(introduction);
        payOrder.setNumber(1);
        payOrder.setState(state);
        payOrderMapper.insorder(payOrder);
        return new Result(1,"订单生成成功",null);
    }

    //查询订单
    @GetMapping("queryOrder")
    public Result queryOrder(@RequestParam int uid){
        List<PayOrder> payOrders = payOrderMapper.selOrder(uid);
        return new Result(1,"查询",payOrders);
    }

    @GetMapping("delOrder")
    public Result delOrder(@RequestParam("poid") String poid) {
        int del = payOrderMapper.delOrder(poid);
        if (del ==0){
            return new Result(0, "删除失败", null);
        }else {
            return new Result(1, "删除成功", null);
        }
    }
}
