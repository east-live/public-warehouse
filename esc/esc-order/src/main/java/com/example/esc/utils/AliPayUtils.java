package com.example.esc.utils;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;

public class AliPayUtils {
    public static AlipayClient alipayClient;
    static {
        alipayClient =
                new DefaultAlipayClient(
                        "https://openapi.alipaydev.com/gateway.do",
                        "2021000121669047",
                        "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCa9lF/85Yypm4uibkagDrNadv1tHcxY/HCylGHfTiB3mMzcAWzgsR55OC6cKmRyne3KI3uIPW+AcesK7go/6ORnN0+unJD1sZRye6iEBJrEPuANNHHYzZSXAw9mYdxIN7+5BVgAi+J6vlaW9rBAgklYBhH94s0zXZisAIay1/2rXxERlmop8jLSoX18FqHXlyRdXtuwQdhpPb/uCMr5zkhJczScJHzio+fmx5dzlkAKct1T2oBobYBNAkPKjaT12CFIuMD/MC8t9KSiXgav/jOeNH0Nirc2FOY7VRcSw+M6gBlHRdDb7iBBsKJp0SWWTj9hsPee2zxA4Yc4F+XrS2dAgMBAAECggEAegn1A3y11CvoaDREmA9WwAFXnnjCyh34/zxQ2xCOGREoNRFLHCWnUIwlRA+IK+41PATvShOhFUPXmctQnNE/Y2UThNxXMwAJbc5HZlCcAL/+Iej3MTz8Qf6GEVlBJqIJnNnJz7PI9qgcOsJOVGSjCVFVrg/RDQOm8W/GKI30hJLAqy6ZMXzLYRXzkQlBKySIzW6Hym6evZHa1Iu1QzwyAWPOa2aY68iI8sgiulzU7w+nkY4dBsdJ65KvWtEZIFVUmI+uAtPdovaWBZxh+T93crNVa6jIJpzUd+k0hr0hVjdF2l4S91fzjTkqTyDYsJi56qbH6viO1yvl9ysOtCR0YQKBgQDH8cMwv7OdVluF9cgjdm1iTnFBH2tOYLGcnlvDMzqvMR7RvOCIp+SrP6BBHYRn3HixzEDLjpeljjeG/kvixo260fV2JaiojcbfirNttH6VsgKj8uE2LyOseKT6VhWBlFo+i43cNjDjjSHjYHJpAmd9lANJYofSrh3MzDrqWe9ceQKBgQDGaCGxsjho84ppwfHfV4DtPK0OCYah3UpPQ1NPGQfbI8GFo/qQvbSoyLWwkGjlUf7PbFQcSO1r+0Tepsv8T3w0bdw2ZnnT6PHY4Cd3RKJpOdxPbjzET8xbMeRn2l1VVMKB7Bw3fQd2yA3X8+A570Ar+i7iC4k2MyaTONLIcvEJRQKBgCriB2MlOni8VNTeahZxB/TVriSxgGpHAtD8heHP0v5QIrQJNn8Yw0qXUt+kXQmJl7lKfXsrEgtH/VDOqafxveQbs/GrEglUnxrCvMkCtUOV8ksHhJYCdjw/g+LNI7+u+cwAUmSwmruzQVlrDh7HjTq5zBqpL0hYX7vU8a3hrBzBAoGBAKdpiDzLN+P+06UHPFMLH8OONiupCAwekCwvj+UnJsTW990PD0cAe6cLrJtuTsRpGovlSVeQhZ1eqK8CqQ6d9xy6Ml1IvLV9UkyAX2y44h3N78KqLuYquzGb+LFUxPpSiclc9DYx7pFgnqXAp0vbQBebg+HCgCXWpU8GXH0813vNAoGBALmtBjEZ2mBn5qHyHWq2hbwvPFrYGGKSIpEf+jUI+Xc2Ac/WX1KYLtzFbFDnWAlkziGCDWM2XZ/zUEgGmJCpWUJY/40goN8Gi2oj/eFoUcSwKnTvfSJqEU7TVk6g1KggcJ831iebseqbPM81H5fjPCa6rwRur+saXkWiPalhYKnh",
                        "json",
                        //UTF-8编码格式
                        "UTF-8",
                        "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmvZRf/OWMqZuLom5GoA6zWnb9bR3MWPxwspRh304gd5jM3AFs4LEeeTgunCpkcp3tyiN7iD1vgHHrCu4KP+jkZzdPrpyQ9bGUcnuohASaxD7gDTRx2M2UlwMPZmHcSDe/uQVYAIvier5WlvawQIJJWAYR/eLNM12YrACGstf9q18REZZqKfIy0qF9fBah15ckXV7bsEHYaT2/7gjK+c5ISXM0nCR84qPn5seXc5ZACnLdU9qAaG2ATQJDyo2k9dghSLjA/zAvLfSkol4Gr/4znjR9DYq3NhTmO1UXEsPjOoAZR0XQ2+4gQbCiadEllk4/YbD3nts8QOGHOBfl60tnQIDAQAB",
                        //RSA非对称加密
                        "RSA2");
    }
    public static AlipayClient getAlipayClient(){
        return alipayClient;
    }
}



