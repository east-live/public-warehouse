package com.example.esc.service.impl;

import com.example.esc.entity.CarMessage;
import com.example.esc.mapper.CarMessageMapper;
import com.example.esc.service.ICarMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
@Service
public class CarMessageServiceImpl extends ServiceImpl<CarMessageMapper, CarMessage> implements ICarMessageService {

}
