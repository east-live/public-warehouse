package com.example.esc.controller;

import com.example.esc.entity.CarOrder;
import com.example.esc.mapper.OrderMapper;
import com.example.esc.service.IOrderService;
import com.example.esc.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.time.LocalDate;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
@RestController
@RequestMapping("/esc/order")
public class OrderController {

    @Resource
    private OrderMapper orderMapper;

    @PostMapping("/queryCar")
    public Result queryCar(@RequestParam int uid) {
        return new Result(1, "查询成功", orderMapper.queryCar(uid));
    }


    // 根据oid删除一个订单
    @PostMapping("/deleteOrder")
    public Result deleteOrder(@RequestParam int oid) {
        System.out.println("==========" + oid);
        orderMapper.deleteCarOrderById(oid);
        return new Result(1, "删除成功", null);
    }
    //查询订单信息
    @PostMapping("/carOrderAll")
    public Result carOrderAll(@RequestParam int uid) {
        return new Result(1, "查询成功", orderMapper.selectOrderByUid(uid));
    }
    //全删
    @PostMapping("/deleteOrderAll")
    public Result deleteOrderAll(@RequestParam int[] carIdAll) {
        orderMapper.deleteOrderAll(carIdAll);
        return new Result(1, "删除成功", null);
    }
    // 加入购物车
    @PostMapping("jiarugouwuche")
    public Result jiarugouwuche(@RequestParam("uid") int uid, @RequestParam("carId") int carId, @RequestParam("oldPrice") String oldPrice) {
        CarOrder carOrder1 = orderMapper.queryOrd(uid, carId);
        CarOrder carOrder = new CarOrder();
        if (carOrder1 == null){
            carOrder.setUid(uid);
            carOrder.setCarId(carId);
            carOrder.setPrice(oldPrice);
            carOrder.setState("1");
            carOrder.setTime(LocalDate.now());
            orderMapper.insertcarOrder(carOrder);
            return new Result(1, "添加成功", null);
        }else {
            carOrder1.setNumber(carOrder1.getNumber() + 1);
            orderMapper.UpdatecarOrder(carOrder1.getNumber(),uid,carId);
            return new Result(1, "添加成功", null);
        }

    }

}

