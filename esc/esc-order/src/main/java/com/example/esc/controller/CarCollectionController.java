package com.example.esc.controller;


import com.example.esc.entity.CarCollection;
import com.example.esc.entity.CarMessage;
import com.example.esc.entity.Message;
import com.example.esc.mapper.CarCollectionMapper;
import com.example.esc.vo.Result;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/esc/collection")
public class CarCollectionController {
    @Resource
    private CarCollectionMapper carCollectionMapper;

    @GetMapping("querySc")
    @ResponseBody
    public List<CarCollection> querySc(@RequestParam int uid){
        return carCollectionMapper.selectSc(uid);
    }


    @GetMapping("queryCoCar")
    @ResponseBody
    public List<CarMessage> queryCoCar(@RequestParam int carId){
        return carCollectionMapper.selectCoCar(carId);
    }

    @PostMapping("insertSc")
    @ResponseBody
    public Result insertSC(@RequestParam("uid") int uid,@RequestParam("carId") int carId){
        CarCollection carCollection = new CarCollection();
        carCollection.setCarId(carId);
        carCollection.setUid(uid);
        carCollection.setCarTime(LocalDate.now());
        carCollectionMapper.insertSC(carCollection);
        return new Result(1,"添加成功",null);
    }

    @PostMapping("deleteCo")
    @ResponseBody
    public Result deleteCo(@RequestParam("coid") int coid){


        carCollectionMapper.deleteCo(coid);


        return new Result(1,"删除成功", null);
    }


}
