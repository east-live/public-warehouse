package com.example.esc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.esc.entity.Shouhuodizhi;
import com.example.esc.mapper.ShouhuodizhiMapper;
import com.example.esc.service.IShouhuodizhiService;
import org.springframework.stereotype.Service;

@Service
public class ShouhuodizhiServiceImpl extends ServiceImpl<ShouhuodizhiMapper, Shouhuodizhi> implements IShouhuodizhiService {
}
