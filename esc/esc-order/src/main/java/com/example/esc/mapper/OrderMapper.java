package com.example.esc.mapper;

import com.example.esc.entity.CarMessage;
import com.example.esc.entity.CarOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
public interface OrderMapper extends BaseMapper<CarOrder> {


    @Select("select * from car_message where carId in (select carId from car_order where uid = #{uid})")
    List<CarMessage> queryCar(int uid);

    @Select("select * from car_order where uid = #{uid}")
    List<CarOrder> selectOrderByUid(int uid);

    @Delete("<script>delete from car_order where carId in" +
            " <foreach collection='carId' open='(' item='caiIdAll' separator=',' close=')'>#{caiIdAll}</foreach>" +
            "</script>")
    void deleteOrderAll(@Param("carId") int[] carId);

    @Insert("insert into car_order(uid, carId, time, price, state)" +
            " values(#{uid}, #{carId}, #{time}, #{price}, #{state})")
    void insertcarOrder(CarOrder carOrder);

    @Update("update car_order set number=#{number} where uid=#{uid} and carId=#{carId}")
    void UpdatecarOrder(@Param("number") int number,@Param("uid") int uid,@Param("carId") int carId);

    @Select("select * from car_order where uid=#{uid} and carId=#{carId}")
    CarOrder queryOrd(@Param("uid") int uid,@Param("carId") int carId);

    @Delete("delete from car_order where oid = #{oid}")
    void deleteCarOrderById(int oid);
}
