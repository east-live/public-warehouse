package com.example.esc.mapper;

import com.example.esc.entity.CarMessage;
import com.example.esc.entity.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.esc.entity.User;
import com.example.esc.entity.dto.MessageDto;
import org.apache.ibatis.annotations.*;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
public interface MessageMapper extends BaseMapper<Message> {
    @Select("select * from message where carId=#{carId}")
    @Results(
            value = {
                    @Result(column = "uid", property = "uid", id = true),
                    @Result(column = "uid", property = "user", many = @Many(select = "selectUser")),
            }
    )
    List<Message> liuyan(@Param("carId") int carId);

    @Select("select * from user where uid=#{uid}")
    User selectUser(@Param("uid") String uid);

    @Insert("insert into message(carId, content, uid, time) values(#{carId}, #{content}, #{uid}, #{time})")
    int insertMessage(Message message);

    @Delete("delete from message where mid=#{mid}")
    int delmessage(@Param("mid") int mid);

    // 渲染评论列表
    @Select("select * from message m, car_message cm where m.uid = #{uid} and cm.carId = m.carId")
    List<MessageDto> selectCarMessage(@Param("uid") int uid);
    //删除数据
    @Delete("delete from message where content = #{content} and carId = #{carId}")
    void deleteBymid(@Param("content") String content, @Param("carId") int carId);

}
