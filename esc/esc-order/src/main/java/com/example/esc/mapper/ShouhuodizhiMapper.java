package com.example.esc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.esc.entity.Shouhuodizhi;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface ShouhuodizhiMapper extends BaseMapper<Shouhuodizhi> {
    @Select("select * from shouhuodizhi where uid = #{uid}")
    List<Shouhuodizhi> selectshdzById(int uid);

    void updateshdzBySid(Shouhuodizhi shouhuodizhi);


    // 通过sid删除收货信息
    @Delete("delete  from shouhuodizhi where sid = #{sid}")
    void deleteBySid(int sid);

    // 将所有地址设为默认地址
    @Update("update shouhuodizhi set is_default = 0 where 1=1")
    void updateIsDefaultAll();

    // 设为默认地址
    @Update("update shouhuodizhi set is_default = 1 where sid = #{sid}")
    void updateIsDefaultBySid(int sid);

    // 添加收货地址
    @Insert("insert into shouhuodizhi(username, phone, addr, youbian, uid, xiangxiaddr, is_default)" +
            " values(#{username}, #{phone}, #{addr}, #{youbian}, #{uid}, #{xiangxiaddr}, #{isDefault})")
    void insertshdz(Shouhuodizhi shouhuodizhi);
}
