package com.example.esc.controller;

import com.example.esc.entity.CarOrder;
import com.example.esc.entity.Shouhuodizhi;
import com.example.esc.mapper.OrderMapper;
import com.example.esc.mapper.ShouhuodizhiMapper;
import com.example.esc.vo.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
@RestController
@RequestMapping("/shdz")
public class ShouhuodizhiController {

    @Resource
    private ShouhuodizhiMapper shouhuodizhiMapper;

    //查询收货地址列表
    @GetMapping("/selectshdzById")
    public Result selectshdzById(@RequestParam("uid") int uid) {
        return new Result(1, "查询成功", shouhuodizhiMapper.selectshdzById(uid));
    }

    // 通过sid修改收货信息
    @PostMapping("/updateshdzBySid")
    public Result updateshdzBySid(@RequestBody Shouhuodizhi shouhuodizhi) {
        shouhuodizhiMapper.updateshdzBySid(shouhuodizhi);
        return new Result(1, "修改成功", null);
    }

    // 通过sid删除收货信息
    @GetMapping("/deleteshdz")
    public Result deleteshdz(@RequestParam("sid") int sid) {
        shouhuodizhiMapper.deleteBySid(sid);
        return new Result(1, "删除成功", null);
    }

    // 通过sid删除收货信息
    @GetMapping("/sheweiDefault")
    public Result sheweiDefault(@RequestParam("sid") int sid) {
        shouhuodizhiMapper.updateIsDefaultAll();
        shouhuodizhiMapper.updateIsDefaultBySid(sid);
        return new Result(1, "设置成功", null);
    }

    // 添加收货地址
    @PostMapping("/insertshdz1")
    public Result insertshdz1(@RequestBody Shouhuodizhi shouhuodizhi) {
        shouhuodizhiMapper.insertshdz(shouhuodizhi);
        return new Result(1, "添加成功", null);
    }

}