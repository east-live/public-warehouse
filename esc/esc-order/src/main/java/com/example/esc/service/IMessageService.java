package com.example.esc.service;

import com.example.esc.entity.Message;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
public interface IMessageService extends IService<Message> {

}
