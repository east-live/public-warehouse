package com.example.esc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.esc.entity.CarCollection;
import com.example.esc.entity.CarMessage;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface CarCollectionMapper extends BaseMapper<CarMessage> {


    @Select("select * from car_collection where uid=#{uid}")
    @Results(
            value = {
                    @Result(column = "carId",property = "carId" ,id = true),
                    @Result(column = "carId",property = "CarM" ,many = @Many(select = "selectCoCar"))
            }
    )
    List<CarCollection> selectSc(@Param("uid")int uid);

    @Select("select * from car_message where carId=#{carId}")
    List<CarMessage> selectCoCar(@Param("carId") int carId);

    @Insert("insert into car_collection(coid, uid, carId, car_time) values(#{coid}, #{uid},#{carId}, #{carTime}) ")
    void insertSC(CarCollection carCollection);

    @Delete("delete from car_collection where coid=#{coid}")
    void deleteCo(int coid);



}
