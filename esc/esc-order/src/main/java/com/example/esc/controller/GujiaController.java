package com.example.esc.controller;

import com.example.esc.common.BrandPrice;
import com.example.esc.common.R;

import com.example.esc.entity.GuJiaDto;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;

@RestController
public class GujiaController {

    @PostMapping("/valuebe")
    public R valuebe(GuJiaDto guJiaDto) {
        R r = new R();

        Calendar cal = Calendar.getInstance();
        Double nowYear = cal.get(Calendar.YEAR) * 1.0;
        Double nowMonth = cal.get(Calendar.MONTH) + 1.0;

        Double price = 0.00;
        if (StringUtils.isNotBlank(guJiaDto.getSeries())) { //判定品牌车系
            if (guJiaDto.getSeries().indexOf(BrandPrice.Axi.getDescription()) != -1) {
                price = BrandPrice.Axi.getValue();
            } else if (guJiaDto.getSeries().indexOf(BrandPrice.Qxi.getDescription()) != -1) {
                price = BrandPrice.Qxi.getValue();

            } else if (guJiaDto.getSeries().indexOf(BrandPrice.Txi.getDescription()) != -1) {
                price = BrandPrice.Txi.getValue();
            } else if (guJiaDto.getSeries().indexOf(BrandPrice.Sxi.getDescription()) != -1) {
                price = BrandPrice.Sxi.getValue();
            }
            if (StringUtils.isNotBlank(guJiaDto.getYear())) {
                Double pick = (nowYear - Double.parseDouble(guJiaDto.getYear()));
                if (StringUtils.isNotBlank(guJiaDto.getMonth())) {
                    pick = pick + ((nowMonth - Double.parseDouble(guJiaDto.getMonth())) * 0.1);
                }
                price = price * 0.8; //第一年折价20%
                if (pick > 1)
                    price = price - pick*(Math.pow(0.9, (pick - 1))); //第一年折价20%，往后年每年折价10%，月份安小数位波动
            }
            if (StringUtils.isNotBlank(guJiaDto.getMileage())) {
                String mile = guJiaDto.getMileage() + "00";
                mile = mile.substring(0, 2);
                price = price - price * 0.2 * (Double.parseDouble(mile) * 0.01); //根据行驶里程折价
            }
            if (StringUtils.isNotBlank(guJiaDto.getWron())) {
                price = price * Double.parseDouble(guJiaDto.getWron())*0.1; //根据磨损车堵估价
            }

            price = (double) Math.round(price * 100) / 100;//价格净化为保留两个小数位

            r.setCode(1);
            r.setMsg("该车辆估值为：￥" + price + "万！  我们将会把估值信息发送至您的手机！");

        } else {
            r.setCode(0);
            r.setMsg("估值失败，请详细填写估值信息");
        }

        return r;
    }
}
