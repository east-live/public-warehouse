package com.example.esc.controller;

import com.example.esc.entity.Message;
import com.example.esc.mapper.CarMessageMapper;
import com.example.esc.mapper.MessageMapper;
import com.example.esc.vo.Result;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
@RestController
@RequestMapping("/esc/message")
public class MessageController {

    @Resource
    private MessageMapper messageMapper;


    @PostMapping("liuyan")
    public Result liuyan(@RequestParam("uid") int uid, @RequestParam("carId") int carId, @RequestParam("content") String content) {
        Message message1 = new Message();
        message1.setContent(content);
        message1.setCarId(carId);
        message1.setUid(uid);
        LocalDate date = LocalDate.now();
        message1.setTime(date);
        messageMapper.insertMessage(message1);

        return new Result(1, "留言成功", null);
    }


    @GetMapping("queryliuyan")
    public Result queryliuyan(@RequestParam("carId") int carId) {
        List<Message> liuyan = messageMapper.liuyan(carId);
        return new Result(1, "查询", liuyan);
    }

    @GetMapping("delliuyan")
    public Result delliuyan(@RequestParam("mid") int mid) {
        int del = messageMapper.delmessage(mid);
        if (del ==0){
            return new Result(0, "删除失败", null);
        }else {
            return new Result(1, "删除成功", null);
        }
    }

    // 渲染评论列表
    @GetMapping("/selectCarMessage")
    public Result selectCarMessage(@RequestParam("uid") int uid) {
        return new Result(1, "查询成功", messageMapper.selectCarMessage(uid));
    }
    //删除
    @PostMapping("/deleteBycontent")
    public Result deleteBymid(@RequestParam("content") String content, @RequestParam("carId") int carId) {
        messageMapper.deleteBymid(content, carId);
        return new Result(1, "删除成功", null);
    }

}
