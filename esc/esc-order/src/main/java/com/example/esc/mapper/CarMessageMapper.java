package com.example.esc.mapper;

import com.example.esc.entity.CarMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.esc.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
public interface CarMessageMapper extends BaseMapper<CarMessage> {

    @Select("select * from car_message order by date desc limit 0,12")
    List<CarMessage> selectCar();

    @Select("select * from car_message where carId=#{carId}")
    @Results(
            value = {
                    @Result(column = "uid", property = "uid", id = true),
                    @Result(column = "uid", property = "user", many = @Many(select = "selectUser")),
            }
    )
    CarMessage selectDet(@Param("carId") String carId);

    @Select("select * from user where uid=#{uid}")
    User selectUser(@Param("uid") String uid);

    @Select("select * from car_message order by date desc limit #{start},#{size}")
    List<CarMessage> selectPagesed(@Param("start") int start, @Param("size") int size);

    @Select("select count(*) from car_message")
    long countPage();

    @Select("select * from car_message where type=#{type}")
    List<CarMessage> selectType(@Param("type") String type);

    //发布车源
//    @Insert("insert into car_message(carName, type, old_price,new_price, introduction, date, image, traveled,disp,emission,state, uid)" +
//            " values (#{carName}, #{type}, #{oldPrice},#{newPrice} #{introduction}, #{date}, #{image}, #{traveled},#{disp},#{emission},#{state}, #{uid})")
    void insCarMessage(CarMessage carMessage);
}
