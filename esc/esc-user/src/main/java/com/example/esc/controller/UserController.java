package com.example.esc.controller;

import com.baomidou.mybatisplus.core.toolkit.BeanUtils;
import com.example.esc.entity.User;
import com.example.esc.mapper.UserMapper;
import com.example.esc.vo.BeanConvertMap;
import com.example.esc.vo.Result;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
@Controller
@RequestMapping("/esc/user")
public class UserController {

    @Resource
    private UserMapper userMapper;


    @GetMapping("/getId")
    public int aa() {
        return 1;
    }

    @PostMapping("register")
    @ResponseBody
    public Result register(
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            @RequestParam("phone") String phone){
        //验证用户输入
        if (username == null || username.trim().isEmpty()) {
            return new Result(0, "请输入用户名", null);
        }
        if (password == null || password.trim().isEmpty()) {
            return new Result(0, "请输入密码", null);
        }
        int str = userMapper.register(username, password,phone);
        if (str == 0) {
            return new Result(0, "请输入用户名或密码", null);
        } else {
            return new Result(1, "注冊成功", null);
        }
    }

    @GetMapping("queryuser")
    @ResponseBody
    public User queryuser(@RequestParam("uid") String uid){
        return userMapper.user(uid);
    }

    @PostMapping("changzl")
    @ResponseBody
    public Result changzl(
            @RequestParam("uid") String uid,
            @RequestParam("username") String username,
            @RequestParam(value = "image",required = false) String image,
            @RequestParam("phone") String phone,
            @RequestParam("email") String email,
            @RequestParam("addr") String addr,
            HttpSession session) throws Exception {

        if (image.equals("images/moren.png")){
            int zls = userMapper.changzls(username,phone,email,addr,uid);
            if (zls == 0){
                return null;
            }else {
                User sx = userMapper.user(uid);
                Map<String, Object> user = BeanConvertMap.beanConvertMap(sx);
                session.setAttribute("loginedUser",user);
                return new Result(1,"修改成功",sx);
            }
        }

        int zl = userMapper.changezl(username,image,phone,email,addr,uid);

        if (zl == 0){
            return null;
        }else {
            User sx = userMapper.user(uid);
            Map<String, Object> user = BeanConvertMap.beanConvertMap(sx);
            session.setAttribute("loginedUser",user);
            return new Result(1,"修改成功",sx);
        }
    }

    @GetMapping("/selectshdzById")
    public Result selectshdzById(@RequestParam("uid") int uid) {
        return new Result(1, "查询成功", null);
    }
}
