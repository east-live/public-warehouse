package com.example.esc.vo;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class BeanConvertMap {

    /**
     * 将java对象转成map集合
     * @param bean
     * @return
     * @throws Exception
     */
    public static Map<String, Object> beanConvertMap(Object bean) throws Exception {

        Map<String, Object> dataMap = new HashMap<>();
        // 获取bean的字节码对象
        Class clazz = bean.getClass();
        // 获取对象的所有属性
        Field[] fields = clazz.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            // 获取对象的属性
            Field field = fields[i];
            // 打开私有访问
            field.setAccessible(true);
            // 获取对象的属性名称
            String name = field.getName();
            // 获取对象的属性值
            Object value = field.get(bean);
            // 将对象的属性和属性值封装到map集合中
            dataMap.put(name, value);
        }
        return dataMap;
    }

}
