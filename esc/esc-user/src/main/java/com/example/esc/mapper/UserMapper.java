package com.example.esc.mapper;

import com.example.esc.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
public interface UserMapper extends BaseMapper<User> {

    //注册
    @Insert("insert into user(username, password,phone,image) values(#{username},#{password},#{phone},image)")
    int register(@Param("username") String username, @Param("password") String password, @Param("phone") String phone);

    //更换头像
    @Update("update user set username=#{username}, image=#{image}, phone=#{phone}, email=#{email}, addr=#{addr} where uid=#{uid}")
    int changezl(@Param("username") String username,@Param("image") String image,@Param("phone") String phone,@Param("email") String email, @Param("addr") String addr, @Param("uid") String uid);

    @Update("update user set username=#{username}, phone=#{phone}, email=#{email}, addr=#{addr} where uid=#{uid}")
    int changzls(@Param("username") String username,@Param("phone")String phone,@Param("email") String email, @Param("addr") String addr, @Param("uid") String uid);

    @Select("select * from user where uid=#{uid}")
    User user(@Param("uid") String uid);

//    @Select("select * from shouhuodizhi where uid = #{uid}")
}
