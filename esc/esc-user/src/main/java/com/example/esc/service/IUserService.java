package com.example.esc.service;

import com.example.esc.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author baomidou
 * @since 2023-04-07
 */
public interface IUserService extends IService<User> {

}
