/*
 Navicat Premium Data Transfer

 Source Server         : yc
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : esc

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 23/04/2023 16:19:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for car_collection
-- ----------------------------
DROP TABLE IF EXISTS `car_collection`;
CREATE TABLE `car_collection`  (
  `coid` int(0) NOT NULL AUTO_INCREMENT,
  `uid` int(0) NULL DEFAULT NULL,
  `carId` int(0) NULL DEFAULT NULL,
  `car_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`coid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 98015 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of car_collection
-- ----------------------------
INSERT INTO `car_collection` VALUES (98001, 10001, 53203, '2022-10-13 00:00:00');
INSERT INTO `car_collection` VALUES (98002, 10002, 53204, '2021-10-14 00:00:00');
INSERT INTO `car_collection` VALUES (98003, 10003, 53208, '2020-05-17 00:00:00');
INSERT INTO `car_collection` VALUES (98005, 10005, 53214, '2022-03-18 00:00:00');
INSERT INTO `car_collection` VALUES (98006, 10006, 53217, '2022-04-19 00:00:00');
INSERT INTO `car_collection` VALUES (98007, 10007, 53218, '2022-06-23 00:00:00');
INSERT INTO `car_collection` VALUES (98009, 10011, 53208, '2023-04-19 00:00:00');
INSERT INTO `car_collection` VALUES (98012, 10011, 53203, '2023-04-21 00:00:00');
INSERT INTO `car_collection` VALUES (98013, 10012, 53226, '2023-04-21 00:00:00');
INSERT INTO `car_collection` VALUES (98015, 10013, 53242, '2023-04-23 00:00:00');
INSERT INTO `car_collection` VALUES (98016, 10013, 53226, '2023-04-23 00:00:00');

-- ----------------------------
-- Table structure for car_message
-- ----------------------------
DROP TABLE IF EXISTS `car_message`;
CREATE TABLE `car_message`  (
  `carId` int(0) NOT NULL AUTO_INCREMENT,
  `carName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `old_price` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `new_price` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `introduction` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `date` date NULL DEFAULT NULL,
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `traveled` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `disp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `emission` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `uid` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`carId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53242 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of car_message
-- ----------------------------
INSERT INTO `car_message` VALUES (53201, '大众CC 2020款 330TSI 魅颜版 国VI', '大众', '16.68', '20.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2021-01-01', 'images/dz01.webp', '3.30', '2.0T', '国六', '0', 10001);
INSERT INTO `car_message` VALUES (53202, '大众 豪华甲壳虫', '大众', '2.68', '4.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2020-02-01', 'images/dz02.webp', '2.30', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53203, '大众 凌渡 2019款 230TSI DSG风尚版 ', '大众', '9.68', '13.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2022-02-01', 'images/dz03.webp', '1.30', '2.0T', '国六', '0', 10003);
INSERT INTO `car_message` VALUES (53204, '大众 凌渡 2019款 230TSI DSG风尚版 ', '大众', '9.68', '13.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2020-02-01', 'images/dz04.webp', '4.30', '2.0T', '国六', '0', 10004);
INSERT INTO `car_message` VALUES (53205, '大众 大众CC 2019款 380TSI 曜颜版 ', '大众', '18.68', '24.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2019-06-01', 'images/dz05.webp', '3.90', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53206, '大众 迈特威 2012款 2.0TSI 四驱尊享版', '大众', '21.68', '57.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2015-05-01', 'images/dz06.webp', '17.30', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53207, '大众 迈腾 2016款 1.8TSI 智享豪华型', '大众', '15.68', '24.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2021-05-01', 'images/dz07.webp', '1.50', '2.0T', '国六', '0', 10007);
INSERT INTO `car_message` VALUES (53208, '大众 揽境 2022款 380TSI 四驱豪华佳境版', '大众', '22.68', '34.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2021-08-01', 'images/dz08.webp', '1.90', '2.0T', '国六', '0', 10008);
INSERT INTO `car_message` VALUES (53209, '大众 帕萨特 2013款 1.4TSI DSG尊荣版', '大众', '4.98', '20.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2013-05-01', 'images/dz09.webp', '8.90', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53210, '大众 Passat领驭 2011款 1.8T 自动尊品型', '大众', '3.68', '21.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2011-05-01', 'images/dz10.webp', '11.30', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53211, '大众途观 2012款 2.0TSI 自动四驱菁英版', '大众', '5.68', '28.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2013-05-01', 'images/dz11.webp', '13.30', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53212, '大众 Polo 2009款 Sporty 1.6L 自动版', '大众', '1.68', '13.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2009-05-01', 'images/dz12.webp', '1.30', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53213, '大众 途观 2015款 1.8TSI 自动四驱舒适版', '大众', '7.68', '25.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2015-05-01', 'images/dz13.webp', '5.30', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53214, '大众 朗行 2017款 180TSI DSG舒适版', '大众', '5.68', '14.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2018-05-01', 'images/dz14.webp', '15.30', '2.0T', '国六', '0', 10014);
INSERT INTO `car_message` VALUES (53215, '大众 桑塔纳 2016款 1.6L 手动风尚版', '大众', '4.68', '14.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2015-05-01', 'images/dz15.webp', '11.30', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53216, '奥迪 奥迪Q5L 2018款 40 TFSI 荣享时尚型', '奥迪', '24.68', '41.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2015-05-01', 'images/ad01.webp', '11.30', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53217, '奥迪 奥迪A4L 2020款 40 TFSI 时尚动感型', '奥迪', '19.68', '24.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2011-04-01', 'images/ad02.webp', '2.30', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53218, '奔驰 奔驰GLK级 2015款 GLK 300 4MAT ', '奔驰', '15.68', '48.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2016-04-01', 'images/bc02.webp', '9.30', '2.0T', '国六', '0', 10018);
INSERT INTO `car_message` VALUES (53219, '奔驰 奔驰C级 2019款 C 260 L 运动版', '奔驰', '19.68', '24.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2019-04-01', 'images/bc02.webp', '3.24', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53220, '保时捷 2021款 Cayman 2.0T', '保时捷', '39.68', '56.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2021-05-01', 'images/pc01.webp', '6.24', '2.0T', '国六', '0', 10020);
INSERT INTO `car_message` VALUES (53221, '保时捷 2023款 Boxster 2.0T', '保时捷', '43.68', '67.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2023-01-01', 'images/pc02.webp', '5.14', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53222, '玛莎拉蒂 玛莎拉蒂MC20 2021款 3.0T ', '玛莎拉蒂', '237.68', '318.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2021-02-01', 'images/pc03.webp', '1.24', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53223, '玛莎拉蒂总裁 2020款 3.0T 标准版', '玛莎拉蒂', '79.68', '124.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2020-09-01', 'images/pc04.webp', '2.24', '2.0T', '国六', '0', 10023);
INSERT INTO `car_message` VALUES (53224, '法拉利 S296 2022款 3.0T V6 GTS', '法拉利', '348.68', '424.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2022-04-01', 'images/pc05.webp', '2.29', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53225, '法拉利S296 2021款 3.0T V6 GTB', '法拉利', '298.68', '388.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2021-01-01', 'images/pc06.webp', '3.88', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53226, '迈凯伦GT 2023款 4.0T 标准型', '迈凯伦', '419.68', '524.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2023-01-01', 'images/pc07.webp', '6.84', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53227, '迈凯伦Aurtur 2021款 3.0T PHEV 标准型', '迈凯伦', '499.68', '547.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2021-11-01', 'images/pc08.webp', '2.56', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53228, '迈凯伦Aurtur 2020款 3.0T PHEV 标准型', '迈凯伦', '299.68', '345.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2020-02-01', 'images/pc09.webp', '6.94', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53229, '劳斯莱斯库里南 2020款 Black Badge', '劳斯莱斯', '591.68', '645.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2020-02-01', 'images/pc11.webp', '2.39', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53230, '劳斯莱斯幻影 2018款 6.7T 标准轴距版', '劳斯莱斯', '647.68', '931.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2018-04-01', 'images/pc12.webp', '2.04', '2.0T', '国六', '0', 10030);
INSERT INTO `car_message` VALUES (53231, '阿波罗 Intensa Emozione', '阿波罗', '3299.68', '5345.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2019-04-01', 'images/pc13.webp', '0.64', '2.0T', '国六', '0', 11111);
INSERT INTO `car_message` VALUES (53232, '阿波罗 Arrow', '阿波罗', '4299.68', '5345.88', '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2020-04-01', 'images/pc14.webp', '0.82', '2.0T', '国六', '0', 11111);

-- ----------------------------
-- Table structure for car_order
-- ----------------------------
DROP TABLE IF EXISTS `car_order`;
CREATE TABLE `car_order`  (
  `oid` int(0) NOT NULL AUTO_INCREMENT,
  `uid` int(0) NULL DEFAULT NULL,
  `carId` int(0) NULL DEFAULT NULL,
  `time` date NULL DEFAULT NULL,
  `price` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `number` int(0) NULL DEFAULT 1,
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '已付款、提车',
  PRIMARY KEY (`oid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 66026 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of car_order
-- ----------------------------
INSERT INTO `car_order` VALUES (66002, 10002, 53203, '2023-01-12', '9.68', 1, '1');
INSERT INTO `car_order` VALUES (66003, 10005, 53205, '2023-01-11', '18.68', 1, '1');
INSERT INTO `car_order` VALUES (66014, 10001, 53222, '2023-04-15', '237.68', 1, '1');
INSERT INTO `car_order` VALUES (66025, 10011, 53203, '2023-04-21', '9.68', 2, '1');
INSERT INTO `car_order` VALUES (66026, 10013, 53201, '2023-04-23', '16.68', 1, '1');
INSERT INTO `car_order` VALUES (66027, 10011, 53201, '2023-04-23', '16.68', 1, '1');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `mid` int(0) NOT NULL AUTO_INCREMENT,
  `carId` int(0) NULL DEFAULT NULL,
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `uid` int(0) NULL DEFAULT NULL,
  `time` date NULL DEFAULT NULL,
  PRIMARY KEY (`mid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43009 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES (43001, 53201, '这辆车价格还是有一点贵，但是得拿下', 10001, '2023-02-24');
INSERT INTO `message` VALUES (43002, 53203, '这辆车价格还是有一点贵，但是得拿下', 10002, '2023-01-24');
INSERT INTO `message` VALUES (43003, 53209, '这辆车价格还是有一点贵，但是得拿下', 10005, '2023-01-24');
INSERT INTO `message` VALUES (43004, 53221, 'sss', 10011, '2023-04-18');
INSERT INTO `message` VALUES (43005, 53203, '价格可以面议吗', 10011, '2023-04-21');
INSERT INTO `message` VALUES (43008, 53226, '太贵啦', 10011, '2023-04-22');
INSERT INTO `message` VALUES (43009, 53242, '价格不满意', 10013, '2023-04-23');

-- ----------------------------
-- Table structure for pay_order
-- ----------------------------
DROP TABLE IF EXISTS `pay_order`;
CREATE TABLE `pay_order`  (
  `poid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `uid` int(0) NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `carId` int(0) NULL DEFAULT NULL,
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` date NULL DEFAULT NULL,
  `price` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `number` int(0) NULL DEFAULT 1,
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '已付款、提车',
  PRIMARY KEY (`poid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pay_order
-- ----------------------------
INSERT INTO `pay_order` VALUES ('202342120500429', 10011, '朗普', 53203, '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2023-04-21', '96800', 1, '已付款');
INSERT INTO `pay_order` VALUES ('2023423143615607', 10013, '张三', 53201, '官方认证一汽大众二手车经销商，品质保证放心购买 《发动机》经短途试驾体验，车辆点火 起步 提速 过弯 减速 制动均无问题，加速迅猛，动力输出平稳舒适，无怠速抖动，无重大事故症状。 《整体评估》车况良好，车体骨架结构无变形扭曲，发动机，变速箱工作平稳，请放心购买。 本店支持任何第三方检车！合同承诺非事故车泡水车火烧车等大问题。 车可分期付款，首付两成起，支持0首付，支持两-0利息，一汽大众官方合作金融机构，手续便捷放心', '2023-04-23', '166800', 1, '已付款');

-- ----------------------------
-- Table structure for shouhuodizhi
-- ----------------------------
DROP TABLE IF EXISTS `shouhuodizhi`;
CREATE TABLE `shouhuodizhi`  (
  `sid` int(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `carId` int(0) NULL DEFAULT NULL,
  `youbian` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `uid` int(0) NULL DEFAULT NULL,
  `xiangxiaddr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_default` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`sid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10030 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shouhuodizhi
-- ----------------------------
INSERT INTO `shouhuodizhi` VALUES (10001, '张三', '13621374472', '湖南省衡阳市真香区', NULL, '66666', 10001, '南华大学红湘校区致远园17栋213', 0);
INSERT INTO `shouhuodizhi` VALUES (10012, '李四', '123451354', '湖南省永州市宁远县', NULL, '123456', 10001, '宁远二中新校区', 0);
INSERT INTO `shouhuodizhi` VALUES (10014, '王五', '12345678912', NULL, NULL, '111111', 10001, '大东海北海道', 0);
INSERT INTO `shouhuodizhi` VALUES (10015, '赵六', '147258369321', NULL, NULL, '222222', 10001, '大中国', 0);
INSERT INTO `shouhuodizhi` VALUES (10017, '狂魔哥', '1231231231231', NULL, NULL, '11111', 10001, '出生榜榜首', 0);
INSERT INTO `shouhuodizhi` VALUES (10021, '狂魔哥', '147258369', NULL, NULL, '666', 10001, '出生榜榜首', 0);
INSERT INTO `shouhuodizhi` VALUES (10024, '狂魔哥', '1362146845', '120000450100450105', NULL, '66666', 10001, '出生榜榜首', 0);
INSERT INTO `shouhuodizhi` VALUES (10025, 'ss', '14233525', '310000450200450103', NULL, '23244', 10001, '搜索', 0);
INSERT INTO `shouhuodizhi` VALUES (10026, 'wwa', '34134131353', '120000450100450107', NULL, '3425', 10001, 'af', 0);
INSERT INTO `shouhuodizhi` VALUES (10028, 'sd', '1323', '430000梧州市西乡塘区', NULL, 'sda', 10011, 'ds', 0);
INSERT INTO `shouhuodizhi` VALUES (10029, '张三', '17136496523', '430000北海市江南区', NULL, '424500', 10012, '南华', 0);
INSERT INTO `shouhuodizhi` VALUES (10030, '张三', '17534575363', '北京梧州市青秀区', NULL, '44433', 10013, '南华大学', 1);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `uid` int(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1',
  `addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mid` int(0) NULL DEFAULT NULL,
  `carId` int(0) NULL DEFAULT NULL,
  `sid` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10013 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (10001, '张三丰', 'a', '44356372637@qq.com', '123', NULL, '男', '1', '北京市朝阳区', 43001, 53201, 10001);
INSERT INTO `user` VALUES (10002, '肖先生', '123321', '44356232637@qq.com', '13678907658', NULL, '男', '1', '广州市白云区', 43002, 53204, NULL);
INSERT INTO `user` VALUES (10003, '张女士', '123321', '44356232637@qq.com', '13678907658', NULL, '女', '1', '衡阳市蒸湘区', 43003, 53208, NULL);
INSERT INTO `user` VALUES (10004, '杨先生', '123321', '44356232637@qq.com', '13678907658', NULL, '男', '1', '长沙市天心区', 43004, 53203, NULL);
INSERT INTO `user` VALUES (10005, '王先生', '123321', '44356232637@qq.com', '13678907658', NULL, '男', '1', '娄底市娄心区', 43005, 53214, NULL);
INSERT INTO `user` VALUES (10007, '明先生', '123321', '44356232637@qq.com', '13678907658', NULL, '男', '1', '娄底市娄心区', 43005, 53218, NULL);
INSERT INTO `user` VALUES (10008, '刘先生', '123321', '44356232637@qq.com', '13678907658', NULL, '男', '1', '娄底市娄心区', 43005, 53220, NULL);
INSERT INTO `user` VALUES (10009, '坂本先生', '123321', '44356232637@qq.com', '13678907658', NULL, '男', '1', '北京市朝阳区', 43005, 53207, NULL);
INSERT INTO `user` VALUES (10010, '杰斯先生', '123321', '44356232637@qq.com', '13678907658', NULL, '男', '1', '娄底市娄心区', 43005, 53223, NULL);
INSERT INTO `user` VALUES (10011, '朗普先生', 'a', '44356232637@qq.com', '17322006535', '/upload/f9066874e9e64d6b9570999c48188ce4.jpg', '男', '1', '长沙市天心区', 43005, 53230, NULL);
INSERT INTO `user` VALUES (10013, '张三', 'a', '3038704857@qq.com', '17136496523', '/upload/1628d6539e9741fa9c709b98104faa7a.jpg', NULL, '1', 'null', NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
