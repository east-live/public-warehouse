Vue.component("myframe", {
    props: ["datas"],
    template: `
<div>
 <div class="index_top">
  <div class="index_top_ width_1200_center">

   <div class="index_top_user position_re fl">
   <div v-if="user.length == 0">
    <p>
     <span class="span11 display_n"><a href="user_home.html">
       <span class="span1">许小鞋714</span><span class="span2"></span></a></span>
     <span class="span21"><span class="span3">
       <a href="user_login.html">Hi，请登录</a></span>
      <span class="span4"><a href="register.html">免费注册</a></span></span>
    </p>
    </div>
    
    <div v-else>
    <p>
     <span class="span11 display_n"><a href="user_home.html">
       <span class="span1">许小鞋714</span><span class="span2"></span></a></span>
     <span class="span21"><span class="span3">
       <a href="user_login.html">{{user.username}}</a></span>
      <span class="span4"><a @click.prevent="logOut">退出登录</a></span></span>
    </p>
    </div>
    
   </div>
   
   
   <div class="index_top_serve position_re fr"><span class="serve"><a href="javascript:;">客户服务</a></span>
    <div class="serve_problem position_ab display_n"><a href="#">在线客服</a><a href="#">售后服务</a><a
      href="#">购物指南</a></div>
   </div>
   <div class="index_top_favorite fr"><a :href="'user_home_favorite.html?uid='+ user.uid">收藏夹</a></div>
   <div class="index_top_cart fr"><a href="user_home_cart.html">购物车</a></div>
   <div class="index_top_order fr"><a href="user_home_order.html">我的订单</a></div>
  </div>
 </div>
 <div class="index_logo clearfix">
  <div class="index_logo_ width_1200_center">
   <div class="index_logo_logo fl">
    <a href="javascript:;" title="西瓜二手车电子商务"><img src="images/index_logo_logo2.png" title="西瓜二手车电子商务" /></a>
   </div>
   <div class="index_logo_city fl">
    <div class="index_logo_city_cen">
     <div class="show_hide_city_a position_re">
     </div>

    </div>
   </div>
   <div class="index_logo_search">
    <span class="fr"><input type="text" class="index_logo_search_input" placeholder="请输入要买的品牌"
      name="keyword" autocomplete="off"><button class="search_btn">搜索</button></span>
    <!--1、在div的class中fr会影响到下一个div浮动 就得这样,2、加一个<div class="hr_5"></div>也可以，如购物车页面3、或者加一个overflow-->
   </div>
  </div>
 </div>
 <div class="index_nav1 clearfix">
  <div class="index_nav1_ width_1200_center">
   <div class="index_nav1_home fl"><a href="index.html">首页</a></div>
   <div class="index_nav1_buy fl"><a href="buy.html">买车</a></div>
   <div class="index_nav1_sell fl"><a href="sell.html">卖车</a></div>
   <div class="index_nav1_assess fl"><a href="assess.html">评估</a></div>
   <div class="index_nav1_serve fl"><a href="serve.html">服务保障</a></div>
   <div class="index_nav1_idea fr"><a href="idea.html">意见反馈</a></div>
   <div class="index_nav1_user_home fr"><a :href="'user_home.html?uid='+user.uid">个人中心</a></div>
  </div>
 </div>
 <div class="hr_5"></div>


 <slot>默认插槽</slot>

 <div class="index_foot">
  <div class="index_foot_">
   <div class="index_foot_logo cursor fl"></div>
   <div class="index_foot_phone fl">
    <p>咨询电话:18877829596</p>
    <p>（周一至周日 8:00-23:00）</p>
   </div>
   <div class="index_foot_copyright fr">
    <p class="p1"><a href="#">网站首页</a><a href="#">关于西瓜</a><a href="#">加入我们</a><a href="#">联系我们</a></p>
    <p class="p2 p3">版权所有：河池学院&nbsp;地址：广西河池市宜州市龙江路42号&nbsp;邮箱：536400</p>
    <p class="p2">CopyRight&nbsp;&copy;
     <!--©-->&nbsp;2016&nbsp;BitAuto,All&nbsp;Rights&nbsp;Reserved
    </p>
   </div>
  </div>
 </div>
 <div class="index_problem">
  <div class="index_problem_ width_1200_center">
   <ul>
    <li>
     <h1><a href="#">购物指南</a></h1>
     <ul>
      <li><a href="#">购物流程</a></li>
      <li><a href="#">会员介绍</a></li>
      <li><a href="#">常见问题</a></li>
      <li><a href="#">联系客服</a></li>
     </ul>
    </li>
    <li>
     <h1><a href="#">配送方式</a></h1>
     <ul>
      <li><a href="#">上门自提</a></li>
      <li><a href="#">配送费收标准</a></li>
     </ul>
    </li>
    <li>
     <h1><a href="#">支付方式</a></h1>
     <ul>
      <li><a href="#">货到付款</a></li>
      <li><a href="#">在线支付</a></li>
      <li><a href="#">分期付款</a></li>
     </ul>
    </li>
    <li>
     <h1><a href="#">售后服务</a></h1>
     <ul>
      <li><a href="#">售后政策</a></li>
      <li><a href="#">退款说明</a></li>
      <li><a href="#">取消订单</a></li>
     </ul>
    </li>
    <li>
     <h1><a href="#">西瓜特色服务</a></h1>
     <ul>
      <li><a href="#">售后政策</a></li>
      <li><a href="#">退款说明</a></li>
      <li><a href="#">取消订单</a></li>
     </ul>
    </li>
   </ul>
  </div>
 </div>
</div>


    `
    ,
    data() {
        return {
            cate: [],
            user: []
        }
    },
    created() {
        this.getLoginedUser();
    },
    methods: {

        getLoginedUser() {
            axios.get("/esc-user/getLoginedUser").then(res => {
                if (res.data.code) {
                    this.user = res.data.data;
                    this.$emit("user", this.user)
                }
            })
        },
        logOut() {
            axios.get("/esc-user/logout").then(res => {
                if (res.data.code) {
                    this.user = [];
                    alert(res.data.msg)

                }
            })
        },
    }
});